let defaults = (testObject, defaultProps) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  for (let key in defaultProps) {
    if (testObject[key] !== undefined) {
      continue;
    } else {
      testObject[key] = defaultProps[key];
    }
  }

  return testObject;
};

module.exports = defaults;
