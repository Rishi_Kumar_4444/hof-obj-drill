let invert = (testObject) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  let newObject = {};

  for (let key in testObject) {
    newObject[testObject[key]] = key;
  }

  return newObject;
};

module.exports = invert;
