let keys = (testObject) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  let objKeys = [];

  for (let key1 in testObject) {
    objKeys.push(key1);
  }

  if (objKeys.length > 0) {
    return objKeys;
  } else {
    return "object is empty";
  }
};

module.exports = keys;
