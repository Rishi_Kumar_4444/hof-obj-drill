let mapObject = (testObject, cb) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  try {
    for (let key in testObject) {
      if (testObject[key] == undefined) {
        testObject[key] = null;
      } else {
        let newValue = cb(testObject[key]);

        testObject[key] = newValue;
      }
    }

    return testObject;
  } catch (err) {
    return `${err} is occured`;
  }
};

module.exports = mapObject;
