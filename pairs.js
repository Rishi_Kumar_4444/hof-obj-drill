let pairs = (testObject) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  let pairsArr = [];

  for (let key in testObject) {
    pairsArr.push([key, testObject[key]]);
  }

  if (pairsArr.length > 0) {
    return pairsArr;
  } else {
    return "object is not found";
  }
};

module.exports = pairs;
