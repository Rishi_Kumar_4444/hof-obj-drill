const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const mapObject = require("../mapObject.js");

let cb = (value) => {
  return value + 20;
};

const result = mapObject(testObject, cb);

console.log(result);
