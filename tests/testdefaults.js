const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const defaults = require("../defaults.js");

const result = defaults(testObject, {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  country: "india",
});

console.log(result);
