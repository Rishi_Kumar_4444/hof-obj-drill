let values = (testObject) => {
  if (
    typeof testObject !== "object" ||
    Array.isArray(testObject) ||
    testObject == null
  ) {
    return "input is not an object";
  }

  let valuesArr = [];

  for (let key in testObject) {
    if (testObject[key] == undefined) {
      valuesArr.push(null);
    }

    valuesArr.push(testObject[key]);
  }

  if (valuesArr.length > 0) {
    return valuesArr;
  } else {
    return "object is empty";
  }
};

module.exports = values;
